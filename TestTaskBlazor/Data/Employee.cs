﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskBlazor.Data
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Salary { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
